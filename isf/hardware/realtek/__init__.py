from isf.core import logger
import isf.hardware.ttltalker
import time
from .config import prefixes
import re
import os


class Realtek:
    baudrate = 115200
    com_port = '/dev/ttyACM0'
    uart_obj = None
    timeout = 0.1
    newline_bytes = '0a0d'
    turn_off_time = 5
    init_console_bytes = '1b'
    console_status = 0
    current_prefix = 'NOTEXIST'
    cmd_list_array = []

    def __init__(self, com_port="/dev/ttyUSB0", baudrate=115200, timeout=0.01,
                 newline_bytes="0a0d", turn_on_time=10,
                 init_console_bytes="1b", init_console=True, cmd_prefix="00"):
        self.baudrate = baudrate
        self.com_port = com_port
        self.timeout = timeout
        self.newline_bytes = bytes.fromhex(newline_bytes).decode('charmap')
        self.turn_on_time = turn_on_time
        self.init_console_bytes = init_console_bytes
        self.newline_bytes_hex = newline_bytes
        self.console_status = not init_console
        if cmd_prefix == '00':
            self.cmd_prefix = prefixes
        else:
            self.cmd_prefix = [bytes.fromhex(cmd_prefix).decode('charmap')]
        self.create_connection()

    def create_connection(self):
        self.uart_obj = isf.hardware.ttltalker.TTLTalker(self.com_port,
                                                         self.baudrate,
                                                         self.timeout)
        if not self.uart_obj:
            logger.error("Can't connect to UBoot console!")
            return -1

    def init_console(self):
        if not self.uart_obj:
            result = self.create_connection()
            if result == -1:
                return -1
        logger.info(
            "Please, turn off IoT device and wait for {} seconds!".format(
                self.turn_off_time))
        time.sleep(self.turn_off_time)
        start_time = time.time()
        print_once = 0
        while time.time() - start_time < self.turn_on_time + 5:  # 5 - turn on human time
            if not print_once:
                logger.info("Turn on IoT device.. NOW!")
                print_once = 1
            self.uart_obj.send_bytes(self.init_console_bytes, True,
                                     100, '')
        self.console_status = self.detect_prefix()

    def detect_prefix(self):
        logger.info("Checking for initiation of console")
        ans = self.uart_obj.send_bytes(self.newline_bytes_hex, True, 10000, '')
        if 'ret_str' in ans:
            logger.debug('Returned: {}'.format(ans['ret_str']))
        else:
            logger.debug('Nothing returned, may be errors!')
            return 0

        for prefix in prefixes:
            if prefix in ans['ret_str'].decode('charmap'):
                self.current_prefix = prefix
                return 1
        logger.error(
            "Can't find U-Boot CLI prefix. If it is not default, change it in config.py of uboot model.")
        return 0

    def clear_console(self):
        # only reading from console
        ans = self.uart_obj.read_output(length=10000)['ret_str']
        ans += self.uart_obj.read_output(length=10000)['ret_str']
        ans += self.uart_obj.read_output(length=10000)['ret_str']
        return ans.decode('charmap')

    def send_cmd(self, cmd="?"):
        if not self.console_status and self.console_status == 0:
            res = self.init_console()
            if res == -1:
                return -1
        elif self.current_prefix == 'NOTEXIST':
            self.detect_prefix()
        # logger.debug('Current prefix: ' + self.current_prefix)
        # logger.debug(self.clear_console())
        # logger.debug("Command: {}".format(cmd))
        # logger.debug("HEX command: {}".format(cmd.encode("charmap").hex()))
        ans = self.uart_obj.send_bytes(cmd.encode("charmap").hex(), output=True,
                                       output_length=10000,
                                       newline_bytes=self.newline_bytes_hex)[
            'ret_str']
        full_result = ans.decode('charmap')
        while not full_result.endswith(
                self.newline_bytes + self.current_prefix):
            # logger.debug(full_result)
            # logger.debug('ITERATION')
            ans = self.uart_obj.read_output(length=10000)['ret_str']
            rest_ans = ans.decode('charmap')
            full_result += rest_ans
            # logger.debug(full_result)
        # logger.debug('RAW cmd answer:')
        # logger.debug(full_result)
        while full_result.endswith(self.newline_bytes + self.current_prefix):
            full_result = full_result[
                          :-len(self.newline_bytes + self.current_prefix)]
        if full_result == cmd:
            full_result = ''
        while full_result.startswith(cmd + self.newline_bytes):
            full_result = full_result[len(cmd + self.newline_bytes):]
        return {'result': full_result}

    def cmd_list(self):
        help_cmd = "?"
        if self.cmd_list_array:
            return {'commands': self.cmd_list_array}
        ans = self.send_cmd(help_cmd)['result']
        # TODO: add regexp for parsing
        arr = ans.split(self.newline_bytes)
        result = {}
        prev = ''
        for line in arr:
            line = line.replace(':', ' ')
            if not line.startswith('-') and not line.startswith(' '):
                result[line.split(' ')[0]] = ''
                if line != line.split(' ')[0]:
                    result[line.split(' ')[0]] = line
                prev = line.split(' ')[0]
            elif line.startswith(' '):
                result[prev] += (result[prev] + ';' + line).strip(';')
        self.cmd_list_array = result
        return {'commands': result}

    def get_envs(self):
        env_cmd = "CP0"
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if env_cmd not in self.cmd_list_array:
            logger.error('Command "{}" not found!')
            return
        ans = self.send_cmd(env_cmd)['result']
        # TODO: add regexp for parsing
        arr = ans.split(self.newline_bytes)
        result = {}
        line_num = 0
        for line in arr:
            line_num += 1
            if '=' not in line:
                logger.error('Error reading {}'.format(line_num))
            else:
                ans = line.split('=')
                result[ans[0]] = ans[1]
        return {'environments': result}

    def ipconfig(self):
        # TODO: only parces values from output
        ip_cmd = "IPCONFIG"
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if ip_cmd not in self.cmd_list_array:
            logger.error('Command "{}" not found!')
            return
        ans = self.send_cmd(ip_cmd)['result']
        # TODO: add regexp for parsing
        arr = ans.split(self.newline_bytes)
        result = {}
        line_num = 0
        for line in arr:
            line_num += 1
            if '=' not in line:
                logger.error('Error reading {}'.format(line_num))
            else:
                ans = line.split('=')
                result[ans[0].strip()] = ans[1]
        return {'environments': result}

    def init_flash(self):
        init_cmd = "FLI"
        detect_size_template = 'size='
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if init_cmd not in self.cmd_list_array:
            logger.error('Command "{}" not found!')
            return
        # TODO: check when several flash-devices connected
        ans = self.send_cmd(init_cmd)['result']
        size = ''
        lines = ans.split(self.newline_bytes)
        for line in lines:
            if detect_size_template in line:
                size = line.split(detect_size_template)[1].split(',')[0]
        digital_size = 0
        if 'GB' in size:
            size = size.replace('GB', '')
            digital_size = int(size) * 1024 * 1024 * 1024
        elif 'MB' in size:
            size = size.replace('MB', '')
            digital_size = int(size) * 1024 * 1024
        elif 'KB' in size:
            size = size.replace('KB', '')
            digital_size = int(size) * 1024
        elif 'B' in size:
            size = size.replace('B', '')
            digital_size = int(size)
        return {'size': digital_size}

    def load_flash_to_ram(self, dst_addr="0x40000", src_addr="0x0",
                          size="0x10"):
        init_cmd = "FLR"
        delay_to_load = 10
        success_str = "Flash Read Successed!"
        if len(self.cmd_list_array) == 0:
            self.cmd_list()
        if init_cmd not in self.cmd_list_array:
            logger.error('Command "{}" not found!')
            return
        cmd = "{} {} {} {}".format(init_cmd, dst_addr, src_addr, size)
        cmd += '\n'
        cmd += 'y'
        logger.info("Wait for loading flash! (10 sec max)")
        tmp_timeout = self.uart_obj.serial_link.timeout
        self.uart_obj.serial_link.timeout = delay_to_load
        ans = self.send_cmd(cmd)['result']
        self.uart_obj.serial_link.timeout = tmp_timeout
        if success_str not in ans:
            logger.error('Error! Cant read memory!')
            return {'status': 1}
        return {'status': 0}

    def memory_dumper(self, dump_type="serial", savepath="1.bin", offset="0x0",
                      length="0x100"):
        if dump_type == 'serial':
            read_cmd = 'DW'
            step = 0x10000
            # TODO: fix later, int <-> hex
            offset = int(offset, 16)
            length = int(length, 16)
            r_get_bytes = "(.+):[ \t]+([0-9a-fA-F]+)[ \t]+([0-9a-fA-F]+)[ \t]+([0-9a-fA-F]+)[ \t]+([0-9a-fA-F]+)"

            logger.info('Starting to dump firmware with serial!')
            if len(self.cmd_list_array) == 0:
                self.cmd_list()
            if read_cmd not in self.cmd_list_array:
                logger.error('Command "{}" not found!')
                return
            real_start = (offset // 0x10) * 0x10
            result_len = 0
            logger.info('Memory page size: 0x{:X}'.format(step))
            page_num = 0
            f = open(savepath, 'wb')
            while result_len < length + (offset % 0x10):
                if length + (offset % 10) - result_len < step * 4:
                    tmp_step = int(
                        (length + (offset % 10) - result_len) * 1.0 / 4.0
                    ) + 1
                    cmd = '{} {:X} {}'.format(read_cmd, real_start, tmp_step)
                else:
                    cmd = '{} {:X} {}'.format(read_cmd, real_start, step)
                ans = self.send_cmd(cmd)['result']
                logger.debug('Len: {}'.format(len(ans)))
                lines = ans.split(self.newline_bytes)
                for line in lines:
                    if re.match(r_get_bytes, line):
                        g = re.match(r_get_bytes, line).groups()
                        tmp_result = b''.join(
                            [bytes.fromhex(x)[::-1] for x in g[1:5]])
                        result_len += len(tmp_result)
                        f.write(tmp_result)
                        real_start += len(tmp_result)
                        if page_num % 100 == 0:
                            logger.info(
                                'Page number {}, Len results: 0x{:X}, Len full: 0x{:X}, last address: {}'.format(
                                    page_num, result_len, length + (
                                            offset % 0x10), g[0]))
                        del tmp_result
                        page_num += 1
                    else:
                        logger.error(
                            'Error with address {}, repeating..'.format(
                                real_start))
                logger.debug(
                    'Len results: 0x{:X}, Len full: 0x{:X}'.format(result_len,
                                                                   length + (
                                                                           offset % 0x10)))
            f.close()
            f = open(savepath, 'rb')
            result = f.read()
            f.close()
            result = result[offset % 0x10:]
            result = result[:length]
            if len(result) != length:
                logger.error(
                    'Something went wrong! (len(result) != length) ')
                return
            f = open(savepath, 'wb')
            f.write(result)
            f.close()
            return {'size': len(result)}

    def firmware_dumper(self, dump_type="serial", savepath="1.bin",
                        offset="0x40000"):

        result = self.init_flash()
        if 'size' not in result:
            logger.error('Can\'t get flash size!')
            return
        size = result['size']
        logger.info('Dumping flash with size ' + hex(size))
        self.load_flash_to_ram(offset, '0x0', hex(size))
        # TODO: add not only hex
        return self.memory_dumper(dump_type, savepath, offset, hex(size))
